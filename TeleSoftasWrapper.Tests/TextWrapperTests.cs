﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using TeleSoftasTextWrapper;
using Xunit;

namespace TeleSoftasWrapper.Tests
{
    public class TextWrapperTests
    {
        private readonly TextWrapper _textWrapper;

        public TextWrapperTests()
        {
            _textWrapper = new TextWrapper();
        }

        [Fact]
        public async Task WrapLines_MaxLineLengthBiggerThanWholeLine_OutputsUntouchedLine()
        {
           await AssertLineWrapping(
               maxLineLength: 9999, 
               input: new[] { "Hello world!" }, 
               expectedStrings: new[] { "Hello world!" }
            );
        }

        [Fact]
        //Multiple words, whole line exceeds max length, none of the words exceeds max length individually
        public async Task WrapLines_LineExceedsMaxLength_AllWordsAreShorter_OutputsWrappedLine()
        {
            await AssertLineWrapping(
                maxLineLength: 5,
                input: new[] {"This is kinda long"},
                expectedStrings: new[] {"This", "is", "kinda", "long"}
            );
        }

        [Fact]
        public async Task WrapLines_LineExceedsmaxLength_SingleWord()
        {
            await AssertLineWrapping(
                maxLineLength: 9,
                input: new[] { "Honorificabilitudinitatibus" },
                expectedStrings: new[] { "Honorific", "abilitudi", "nitatibus" }
            );
        }

        [Fact]
        public async Task WrapLines_LineExceedsMaxLength_WordsAreLonger_OutputsWrappedLine()
        {
            await AssertLineWrapping(
                maxLineLength: 5,
                input: new[] { "This sentence sure is a long one" },
                expectedStrings: new[] { "This", "sente", "nce", "sure", "is a", "long", "one" }
            );
        }

        [Fact]
        public async Task WrapLines_MultipleLines_VariousLengths()
        {
            await AssertLineWrapping(
                maxLineLength: 3,
                input: new[] { "1234567", "1", "1234" },
                expectedStrings: new[] { "123", "456", "7 1", "123", "4" }
            );
        }

        private async Task AssertLineWrapping(int maxLineLength, string[] input, string[] expectedStrings)
        {
            var outputSource = new BufferBlock<string>();

            //Act
            _textWrapper.WrapLines(maxLineLength, input, outputSource);

            var outputFileLines = new List<string>();
            while (await outputSource.OutputAvailableAsync())
            {
                var line = await outputSource.ReceiveAsync();
                outputFileLines.Add(line);
            }

            //Assert
            Assert.Equal(expectedStrings.Length, outputFileLines.Count);

            for (var i = 0; i < expectedStrings.Length; i++)
            {
                Assert.Equal(expectedStrings[i], outputFileLines[i]);
            }
        }
    }
}