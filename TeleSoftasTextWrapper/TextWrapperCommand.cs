﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using ManyConsole;

namespace TeleSoftasTextWrapper
{
    public class TextWrapperCommand : ConsoleCommand
    {
        public int MaxLineLength { get; set; }
        public string InputFileLocation { get; set; }
        public string OutputFileLocation { get; set; }

        public TextWrapperCommand()
        {
            IsCommand("wrap-text", "Wraps text to lines that have defined length");
            HasRequiredOption<int>("maxLineLength=", "Maximum amount of characters in line", x => MaxLineLength = x);
            HasRequiredOption("inputFileLocation=", "Path to file that contains text which is going to be wrapped", x => InputFileLocation = x);
            HasRequiredOption("outputFileLocation=", "Path to file that will contain wrapped text", x => OutputFileLocation = x);
        }

        public override int Run(string[] remainingArguments)
        {
            try
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                var textWrapper = new TextWrapper();
                var inputFileLines = ReadLinesLazily(InputFileLocation);

                var outputSource = new BufferBlock<string>();

                WriteToFileAsync(OutputFileLocation, outputSource);
                textWrapper.WrapLines(MaxLineLength, inputFileLines, outputSource).Wait();

                stopwatch.Stop();
                Console.WriteLine($"Finished wrapping text in {stopwatch.ElapsedMilliseconds} miliseconds");
                Console.ReadLine();

                return 0;
            }
            //TODO: catch specific exceptions
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private IEnumerable<string> ReadLinesLazily(string inputFileLocation)
        {
            Console.WriteLine($"Reading input file at {InputFileLocation}");
            var fileLines = File.ReadLines(inputFileLocation);
            return fileLines;
        }

        private async Task WriteToFileAsync(string outputFileLocation, ISourceBlock<string> outputSource)
        {
            Console.WriteLine("Started writing output");
            using (var streamWriter = new StreamWriter(outputFileLocation))
            {
                while (await outputSource.OutputAvailableAsync())
                {
                    var outputLine = await outputSource.ReceiveAsync();
                    await streamWriter.WriteLineAsync(outputLine);
                }
            }
        }
    }
}
