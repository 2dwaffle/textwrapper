﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TeleSoftasTextWrapper
{
    public class TextWrapper
    {
        public async Task WrapLines(int maxLineLength, IEnumerable<string> inputFileLines, ITargetBlock<string> outputSource)
        {
            var leftoverWord = string.Empty;

            foreach (var inputFileLine in inputFileLines)
            {
                var trimmedInputFileLine = leftoverWord + inputFileLine.Trim();
                if (trimmedInputFileLine.Length <= maxLineLength)
                {
                    outputSource.Post(trimmedInputFileLine);
                    leftoverWord = string.Empty;
                    continue;
                }

                var words = trimmedInputFileLine.Split(' ');
                leftoverWord = words.Length > 1
                    ? WrapMultipleWords(maxLineLength, outputSource, words)
                    : WrapLongWord(maxLineLength, outputSource, words[0]);

                if (((BufferBlock<string>) outputSource).Count > 50000)
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(100));
                }
            }

            if (leftoverWord.Length > 0)
            {
                leftoverWord = leftoverWord.Trim();
                outputSource.Post(leftoverWord);
            }

            outputSource.Complete();
        }

        private static string WrapMultipleWords(int maxLineLength, ITargetBlock<string> outputSource, string[] words)
        {
            var line = string.Empty;
            foreach (var word in words)
            {
                var intermediateWordValue = string.Empty;
                if (word.Length > maxLineLength)
                {
                    if (line.Length > 0)
                    {
                        line = line.Trim();
                        outputSource.Post(line);
                        line = string.Empty;
                    }

                    intermediateWordValue = WrapLongWord(maxLineLength, outputSource, word);
                }
                
                intermediateWordValue = string.IsNullOrWhiteSpace(intermediateWordValue)
                    ? word
                    : intermediateWordValue;

                if (line.Length + intermediateWordValue.Length - 1 <= maxLineLength)
                {
                    if (intermediateWordValue.Contains(' '))
                    {
                        line += $"{intermediateWordValue}";
                        continue;
                    }

                    line += $"{intermediateWordValue} ";
                    continue;
                }

                line = line.Trim();
                outputSource.Post(line);
                line = $"{intermediateWordValue} ";
            }

            return line.Length > 0
                ? line
                : string.Empty;
        }

        private static string WrapLongWord(int maxLineLength, ITargetBlock<string> outputSource, string word)
        {
            while (word.Length > maxLineLength)
            {
                var wordPart = word.Substring(0, maxLineLength);
                outputSource.Post(wordPart);
                word = word.Substring(maxLineLength);
            }

            return $"{word} ";
        }
    }
}
