﻿using System;
using System.Collections.Generic;
using ManyConsole;

namespace TeleSoftasTextWrapper
{
    class Program
    {
        public static int Main(string[] args)
        {
            var consoleCommands = GetCommands();

            return ConsoleCommandDispatcher.DispatchCommand(consoleCommands, args, Console.Out);
        }

        public static IEnumerable<ConsoleCommand> GetCommands()
        {
            return ConsoleCommandDispatcher.FindCommandsInSameAssemblyAs(typeof(Program));
        }
    }
}
