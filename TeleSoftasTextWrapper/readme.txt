Wrapper can be run by calling exe file from console and appending such text:
	wrap-text -maxLineLength [desired length] inputFileLocation [full path to input file] -outputFileLocation [full path to output file]

E.g.: C:\Desktop\TeleSoftasTextWrapper.exe wrap-text -maxLineLength 3 inputFileLocation "C:\Desktop\input.txt" -outputFileLocation "C:\Desktop\output.txt"

In order to debug in Visual Studio:
	1. Right click on TeleSoftasTextWrapper project
	2. Select "Debug"
	3. Find textbox with label "Application arguments"
	4. Write such text in it: wrap-text -maxLineLength [desired length] inputFileLocation [full 	path to input file] -outputFileLocation [full path to output file]
		E.g.: wrap-text -maxLineLength 3 inputFileLocation "C:\Desktop\input.txt" -outputFileLocation "C:\Desktop\output.txt"